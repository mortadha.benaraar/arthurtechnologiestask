using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Scale : MonoBehaviour
{
    [SerializeField]
    float RotateSpeed;
    [SerializeField]
    float ScaleValue;
    [SerializeField]
    GameObject Child;


    /// <summary>
    /// Rotate The GameObject Around itself
    /// </summary>
    void Rotate()
    {
        float RotateX = Input.GetAxis("Mouse X") * RotateSpeed * Mathf.Deg2Rad;
  
        float RotateY = Input.GetAxis("Mouse Y") * RotateSpeed * Mathf.Deg2Rad;
    
        if(Input.GetMouseButton(0) && Input.GetAxis("Mouse X") <=1f)
        {
            transform.RotateAround(Child.transform.position,Vector3.up, -RotateX);
        }
        if(Input.GetMouseButton(0) && Input.GetAxis("Mouse Y") <= 1f)
        {
            transform.RotateAround(Child.transform.position,Vector3.right, RotateY);
        }
        
    }

    /// <summary>
    /// Scale The Gamobject Along the Axis 
    /// </summary>
    void Stretch()
    {
        Vector3 temp = transform.localScale;
        temp.x += Input.GetAxis("Mouse X") * Time.deltaTime *ScaleValue;
        temp.y += Input.GetAxis("Mouse Y") * Time.deltaTime *ScaleValue;
        Vector3 pos = transform.forward;
        if (Input.GetMouseButton(1))
        {
            transform.localScale = new Vector3(temp.x, temp.y,pos.z);
        }
    }

    private void Update()
    {
        Rotate();
        Stretch();
    }
}
